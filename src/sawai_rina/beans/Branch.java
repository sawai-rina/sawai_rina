package sawai_rina.beans;

import java.io.Serializable;
import java.util.Date;

public class Branch implements Serializable {
    private static final long serialVersionUID = 1L;


    private int id;
    private String branchs_name;

    private Date createdDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBranchs_name() {
		return branchs_name;
	}

	public void setBranchs_name(String branchs_name) {
		this.branchs_name = branchs_name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}