package sawai_rina.beans;

import java.io.Serializable;
import java.util.Date;

public class Division implements Serializable {
    private static final long serialVersionUID = 1L;



    private int id;
 
    private String divisions_name;

    private Date createdDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDivisions_name() {
		return divisions_name;
	}

	public void setDivisions_name(String divisions_name) {
		this.divisions_name = divisions_name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}