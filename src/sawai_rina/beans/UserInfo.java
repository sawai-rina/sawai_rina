package sawai_rina.beans;

import java.io.Serializable;
import java.util.Date;

public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String name;
    private String password;
    private int branch;
    private int division;
    private String branchs_name;
    private String divisions_name;
    private Date created_at;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public int getDivision() {
		return division;
	}
	public void setDivision(int division) {
		this.division = division;
	}
	public String getBranchs_name() {
		return branchs_name;
	}
	public void setBranchs_name(String branchs_name) {
		this.branchs_name = branchs_name;
	}
	public String getDivisions_name() {
		return divisions_name;
	}
	public void setDivisions_name(String divisions_name) {
		this.divisions_name = divisions_name;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;

	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}





}
