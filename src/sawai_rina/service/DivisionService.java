package sawai_rina.service;

import static sawai_rina.utils.CloseableUtil.*;
import static sawai_rina.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sawai_rina.beans.Division;
import sawai_rina.dao.DivisionDao;

public class DivisionService {

    public List<Division> getDivision() {

        Connection connection = null;
        try {
            connection = getConnection();

           DivisionDao divisionDao = new DivisionDao();
           List<Division> ret = divisionDao.select(connection);
           commit(connection);

           return ret;

           } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}