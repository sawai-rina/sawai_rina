package sawai_rina.service;

import static sawai_rina.utils.CloseableUtil.*;
import static sawai_rina.utils.DBUtil.*;

import java.sql.Connection;

import sawai_rina.beans.User;
import sawai_rina.dao.UserDao;
import sawai_rina.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
        }
        public void update(User user) {

            Connection connection = null;
            try {
                connection = getConnection();

                String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);

                UserDao userDao = new UserDao();
                userDao.update(connection, user);

                commit(connection);
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
        }
    }
