package sawai_rina.service;

import static sawai_rina.utils.CloseableUtil.*;
import static sawai_rina.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sawai_rina.beans.Branch;
import sawai_rina.dao.BranchDao;

public class BranchService {

    public List<Branch> getBranch() {

        Connection connection = null;
        try {
            connection = getConnection();

           BranchDao branchDao = new BranchDao();
           List<Branch> ret = branchDao.select(connection);
           commit(connection);

           return ret;

           } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
       }
       }

    }