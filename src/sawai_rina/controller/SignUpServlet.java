package sawai_rina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sawai_rina.beans.Branch;
import sawai_rina.beans.Division;
import sawai_rina.beans.User;
import sawai_rina.service.BranchService;
import sawai_rina.service.DivisionService;
import sawai_rina.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

        List<Branch> branch = new BranchService().getBranch();
        request.setAttribute("branch", branch);

        List<Division> division = new DivisionService().getDivision();
        request.setAttribute("division", division);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();

            user.setLoginId(request.getParameter("login_id"));
            user.setName(request.getParameter("name"));
            user.setPassword(request.getParameter("password"));
            user.setPassword1(request.getParameter("password1"));

            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setDivision(Integer.parseInt(request.getParameter("division")));


            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

	private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String password1 = request.getParameter("password1");


        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ユーザーIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if(!login_id.matches("^[a-zA-Z0-9]{6,20}$")) {
            messages.add("ユーザーIDは半角英数字の6文字以上20文字以下で入力してください");
        }
        if(!password.matches("^[a-zA-Z0-9 -+*]{6,20}$")) {
            messages.add("パスワードは記号を含む全ての半角英数字の6文字以上20文字以下で入力してください");
        } else if (!password1.equals(password)) {
        	messages.add("パスワードが一致していません。パスワードと確認パスワードは同じものを入力してください");
        }
        if(name.length() >10) {
			 messages.add("ユーザー名は10文字以下で入力してください");
        }

        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
	}
}


