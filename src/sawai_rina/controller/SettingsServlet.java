package sawai_rina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sawai_rina.beans.Branch;
import sawai_rina.beans.Division;
import sawai_rina.beans.User;
import sawai_rina.beans.UserInfo;
import sawai_rina.service.BranchService;
import sawai_rina.service.DivisionService;
import sawai_rina.service.UserInfoService;
import sawai_rina.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)
    		throws ServletException, IOException {

        int value = Integer.parseInt(request.getParameter("Id"));
        UserInfo edit = new UserInfoService().getEdit(value);
        request.setAttribute("edit", edit);

        List<Branch> branch = new BranchService().getBranch();
        request.setAttribute("branch", branch);

        List<Division> division = new DivisionService().getDivision();
        request.setAttribute("division", division);


        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        List<Branch> branch = new BranchService().getBranch();
        List<Division> division = new DivisionService().getDivision();

        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            new UserService().update(editUser);
            response.sendRedirect("./");

        } else {
            request.setAttribute("branch", branch);
         	request.setAttribute("division", division);
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
        throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("Id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setName(request.getParameter("name"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setPassword(request.getParameter("password1"));
        editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
        editUser.setDivision(Integer.parseInt(request.getParameter("division")));

        return editUser;
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String password1 = request.getParameter("password1");

        UserInfo edit = new UserInfoService().getLogin_id(login_id);

        if(!login_id.matches("^[a-zA-Z0-9]{6,20}$")) {
        	messages.add("ログインIDは半角英数字で6文字以上20文字以下です");
        }

        if(!(edit != null)) {
        	messages.add("そのアカウントは存在しています");
        }

        if (StringUtils.isEmpty(password) == true) {
        } else if(!password.matches("^[a-zA-Z0-9 -~]{6,20}$")) {
        	messages.add("パスワードは半角文字で6文字以上20文字以下です");
        } else if(!password1.equals(password)) {
        	messages.add("パスワードが一致していません");
        }

        if(name.length() > 10) {
        	messages.add("ユーザー名は10文字以内で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}

