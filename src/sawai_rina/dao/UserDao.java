package sawai_rina.dao;

import static sawai_rina.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;

import sawai_rina.beans.User;
import sawai_rina.exception.NoRowsUpdatedRuntimeException;
import sawai_rina.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branch");
            sql.append(", division");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // name
            sql.append(", ?"); // password
            sql.append(", ?"); // branch
            sql.append(", ?"); // division
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getDivision());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", name = ?");
            sql.append(", branch = ?");
            sql.append(", division = ?");
            if(StringUtils.isEmpty(user.getPassword()) == false) {
            	 sql.append(", password = ?");
            }
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getName());
            ps.setInt(3, user.getBranch());
            ps.setInt(4, user.getDivision());
            if(StringUtils.isEmpty(user.getPassword()) == false) {
            	ps.setString(5, user.getPassword());
            	ps.setInt(6, user.getId());
            } else {
            	ps.setInt(5,user.getId());
            }




            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }


}