package sawai_rina.dao;

import static sawai_rina.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sawai_rina.beans.Division;
import sawai_rina.exception.SQLRuntimeException;

public class DivisionDao {

	public List<Division> select(Connection connection) {

        PreparedStatement ps = null;

        //divisionsテーブル
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("divisions.id as id, ");
            sql.append("divisions.divisions_name as divisions_name ");
            sql.append("FROM divisions ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Division> ret = toDivisionList(rs);
            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<Division> toDivisionList(ResultSet rs)
            throws SQLException {

        List<Division> ret = new ArrayList<Division>();
        try {
            while (rs.next()) {
                String divisions_name = rs.getString("divisions_name");
                int id = rs.getInt("id");

                Division division = new Division();
                division.setDivisions_name(divisions_name);
                division.setId(id);

                ret.add(division);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}