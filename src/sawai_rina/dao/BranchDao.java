package sawai_rina.dao;

import static sawai_rina.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sawai_rina.beans.Branch;
import sawai_rina.exception.SQLRuntimeException;

public class BranchDao {
	
	public List<Branch> select(Connection connection) {

        PreparedStatement ps = null;
        
        try {
        	//branchsテーブル
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("branchs.id as id, ");
            sql.append("branchs.branchs_name as branchs_name ");
            sql.append("FROM branchs ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Branch> ret = toBranchList(rs);
            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<Branch> toBranchList(ResultSet rs)
            throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                String branchs_name = rs.getString("branchs_name");
                int id = rs.getInt("id");

                Branch branch = new Branch();
                branch.setBranchs_name(branchs_name);
                branch.setId(id);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}