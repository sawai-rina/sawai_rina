package sawai_rina.dao;

import static sawai_rina.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sawai_rina.beans.UserInfo;
import sawai_rina.exception.SQLRuntimeException;


public class UserInfoDao {


		public List<UserInfo>getUserInfo(Connection connection) {

	    PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("SELECT ");
	        sql.append("users.id as id, ");
	        sql.append("users.login_id as login_id, ");
	        sql.append("users.name as name, ");
	        sql.append("users.password as password, ");
	        sql.append("branchs.branchs_name as branchs_name, ");
	        sql.append("divisions.divisions_name as divisions_name ");
	        sql.append("FROM users ");
	        sql.append("INNER JOIN branchs ");
	        sql.append("ON users.branch = branchs.id ");
	        sql.append("INNER JOIN divisions ");
	        sql.append("ON users.division = divisions.id ");

	        ps = connection.prepareStatement(sql.toString());

	        ResultSet rs = ps.executeQuery();
	        List<UserInfo> ret = toUserInfoList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	          close(ps);
	        }
	    }

	       private List<UserInfo> toUserInfoList(ResultSet rs)
	            throws SQLException {

	        List<UserInfo> ret = new ArrayList<UserInfo>();
	          try {
	            while (rs.next()) {
	               int id = rs.getInt("id");
	               String login_id = rs.getString("login_id");
	               String name = rs.getString("name");
	               String password = rs.getString("password");
	               String branchsName = rs.getString("branchs_name");
	               String divisionsName = rs.getString("divisions_name");


	               UserInfo userinfo = new UserInfo();
	               userinfo.setId(id);
	               userinfo.setLogin_id(login_id);
	               userinfo.setName(name);
	               userinfo.setPassword(password);
	               userinfo.setBranchs_name(branchsName);
	               userinfo.setDivisions_name(divisionsName);

	               ret.add(userinfo);
	               } return ret;
	               } finally {
	                 close(rs);
	               }
	       }
	       
	       public UserInfo getEdit(Connection connection, int id) {

	    	    PreparedStatement ps = null;
	    	    try {
	    	    	StringBuilder sql = new StringBuilder();
		            sql.append("SELECT ");
		            sql.append("users.id as id, ");
		            sql.append("users.login_id as login_id, ");
		            sql.append("users.password as password, ");
		            sql.append("users.name as name, ");
		            sql.append("users.branch as branch, ");
		            sql.append("users.division as division, ");
		            sql.append("branchs.branchs_name as branchs_name, ");
		            sql.append("divisions.divisions_name as divisions_name ");
		            sql.append("FROM users ");
		            sql.append("INNER JOIN branchs ");
		            sql.append("ON users.branch = branchs.id ");
		            sql.append("INNER JOIN divisions ");
		            sql.append("ON users.division = divisions.id ");
		            sql.append("WHERE users.id = ? ");

	    	        ps = connection.prepareStatement(sql.toString());
	    	        ps.setInt(1, id);

	    	        ResultSet rs = ps.executeQuery();
	    	        List<UserInfo> userinfoList = toUserInfoList(rs);
	    	        if (userinfoList.isEmpty() == true) {
	    	            return null;
	    	        } else if (2 <= userinfoList.size()) {
	    	            throw new IllegalStateException("2 <= userList.size()");
	    	        } else {
	    	            return userinfoList.get(0);
	    	        }
	    	    } catch (SQLException e) {
	    	        throw new SQLRuntimeException(e);
	    	    } finally {
	    	        close(ps);
	    	    }
	    	}
	       private List<UserInfo> toUserEditList(ResultSet rs)
	           throws SQLException {

	    	   List<UserInfo> ret = new ArrayList<UserInfo>();

	    	   try {

	    		   while(rs.next()) {
	    			   int id = rs.getInt("id");
	    			   String login_id = rs.getString("login_id");
	    			   String name = rs.getString("name");
	    			   String password = rs.getString("password");
	    			   int branch = rs.getInt("branch");
	    			   int division = rs.getInt("division");
	    			   String branchs_name = rs.getString("branchs_name");
	    			   String divisions_name = rs.getString("divisions_name");

	    			   UserInfo info = new UserInfo();
	    			   info.setId(id);
	    			   info.setLogin_id(login_id);
	    			   info.setName(name);
	    			   info.setPassword(password);
	    			   info.setBranch(branch);
	    			   info.setDivision(division);
	    			   info.setBranchs_name(branchs_name);
	    			   info.setDivisions_name(divisions_name);

	    			   ret.add(info);

	    		   }
	    		   return ret;
	    	   }finally {
	    		   close(rs);
	    	   }
	       }
	       public UserInfo getLogin_id(Connection connection, String login_id) {

			    PreparedStatement ps = null;
			    try {
			    	 StringBuilder sql = new StringBuilder();
			            sql.append("SELECT ");
			            sql.append("users.login_id as login_id ");
			            sql.append("FROM users ");
			            sql.append("WHERE users.login_id = ? ");

			        ps = connection.prepareStatement(sql.toString());
			        ps.setString(1, login_id);

			        ResultSet rs = ps.executeQuery();
			        List<UserInfo> userList = toUserLogin_idList(rs);

			        if (userList.isEmpty() == true) {
			            return null;
			        } else if (2 <= userList.size()) {
			            throw new IllegalStateException("2 <= userList.size()");
			        } else {
			            return userList.get(0);
			        }
			    } catch (SQLException e) {
			        throw new SQLRuntimeException(e);
			    } finally {
			        close(ps);
			    }
			}

			private List<UserInfo> toUserLogin_idList(ResultSet rs)
		            throws SQLException {

		        List<UserInfo> ret = new ArrayList<UserInfo>();
		        try {
		            while (rs.next()) {
		                String login_id = rs.getString("login_id");

		                UserInfo info = new UserInfo();
		                info.setLogin_id(login_id);

		                ret.add(info);
		            }
		            return ret;
		        } finally {
		            close(rs);
		        }
		    }

	}