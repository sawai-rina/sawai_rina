<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="signup" method="post"><br />
                <label for="login_id">ユーザーID(半角英数字の6文字以上20文字以下)</label>
                <input name="login_id" id="login_id" /><br />
                <label for="name">ユーザー名(10文字以内)</label>
                <input name="name" id="name" /><br />
                <label for="password">パスワード(記号を含む全ての半角英数字6文字以上20文字以下)</label>
                <input name="password" type="password" id="password" /><br>
                <label for="password1">パスワード確認用</label>
                <input name="password1" type="password" id="password1" /> <br />

                <label for = "branch">支店</label>
				<select name = "branch" id = "branch">
			    <c:forEach items="${branch}" var="br">
				<option value="${ br.id }">${ br.branchs_name }" </option>
				</c:forEach>
				</select>
				<br />

	            <label for = "division">部署・役職</label>
				<select name = "division" id = "division">
			    <c:forEach items="${division}" var="dv">
				<option value="${dv.id }">${dv.divisions_name }" </option>
				</c:forEach>
				</select>
				<br />

                <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)Your Name</div>
        </div>
    </body>
</html>